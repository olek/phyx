#!/bin/bash

# Remark: Considere running runtime test suite which is different from
#         this one which is based on the docs.  If this would be implemented
#         we should probably ship all example data and doc in a separate
#         phyx-doc package

set -e

pkg=phyx

if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp -a /usr/share/doc/${pkg}/examples/* "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

cd pxaa2cdn_example && echo pxaa2cdn && pxaa2cdn -a ND6_aa.fa -n ND6_nuc.fa -o CDN_aln.fa >/dev/null && cd ..

cd pxbdfit_example && echo pxbdfit && pxbdfit -t bd.tre -m yule >/dev/null && cd ..

# cd pxbdsim_example && echo pxbdsim && pxbdsim -e 100 -s -b 1 -d 0.5 -o output_tree_file >/dev/null && cd ..

cd pxboot_example && echo pxboot && pxboot -s Alignment -x 112233 -f 0.50 -o output_of_50_jackknife >/dev/null && cd ..

cd pxboot_example && echo pxboot && pxboot -s Alignment -p parts -o output_of_bootstrap >/dev/null && cd ..

cd pxbp_example && echo pxbp && pxbp -t Tree.tre -o bp_output >/dev/null && cd ..

cd pxcat_example && echo pxcat && pxcat -s *.fas *.fa *.phy -p Parts.txt -o Supermatrix.fa >/dev/null && cd ..

cd pxclsq_example && echo pxclsq && pxclsq -s Alignment -p 0.6 >/dev/null && cd ..

cd pxconsq_example && echo pxconsq && pxconsq -s Alignment >/dev/null && cd ..

cd pxcontrates_example && echo pxcontrates && pxcontrates -c contrates_file.txt -t contrates_tree.tre -a 1 >/dev/null 2>/dev/null && cd ..

cd pxfqfilt_example && echo pxfqfilt && pxfqfilt -s fqfilt_test.fastq -m 10 >/dev/null && cd ..

cd pxlog_example && echo pxlog && pxlog -t *.trees -i >/dev/null && cd ..

cd pxlog_example && echo pxlog && pxlog -t *.trees -b 75 -n 2 -o some_output_filename >/dev/null && cd ..

cd pxlssq_example && echo pxlssq && pxlssq -s Alignment >/dev/null && cd ..

cd pxlstr_example && echo pxlstr && pxlstr -t Tree.tre >/dev/null && cd ..

cd pxmrca_example && echo pxmrca && pxmrca -t mrca_test.tre -m mrca.txt >/dev/null && cd ..

cd pxmrcacut_example && echo pxmrcacut && pxmrcacut -t mrca_test.tre -m mrca.txt >/dev/null && cd ..

cd pxmrcaname_example && echo pxmrcaname && pxmrcaname -t mrca_test.tre -m mrca.txt >/dev/null && cd ..

cd pxnj_example && echo pxnj && pxnj -s Alignment.aln >/dev/null && cd ..

cd pxnw_examples && echo pxnw && pxnw -s Alignment.aln >/dev/null && cd ..

cd pxrecode_example && echo pxrecode && pxrecode -s Nucleotide.fa >/dev/null && cd ..

cd pxrevcomp_example && echo pxrevcomp && pxrevcomp -s Nucleotide.fa >/dev/null && cd ..

cd pxrls_example && echo pxrls && pxrls -s test.fa -c oldnames.txt -n newnames.txt >/dev/null && cd ..

cd pxrlt_example && echo pxrlt && pxrlt -t kingdoms.tre -c kingdoms.oldnames.txt -n kingdoms.newnames.txt >/dev/null && cd ..

cd pxrms_example && echo pxrms && pxrms -s Nucleotide.fa -f taxa_to_delete.txt >/dev/null && cd ..

cd pxrmt_example && echo pxrmt && pxrmt -t rmt_test.tre -n s1,s6,s8 >/dev/null && cd ..

cd pxrr_example && echo pxrr && pxrr -t rr_test.tre -g s1,s2 >/dev/null && cd ..

cd pxs2fa_pxs2phy_pxs2nex_example && \
   echo pxs2fa && pxs2fa -s Alignment >/dev/null && \
   echo pxs2phy && pxs2phy -s Alignment >/dev/null && \
   echo pxs2nex && pxs2nex -s Alignment >/dev/null && \
   cd ..

cd pxseqgen_example && echo pxseqgen && pxseqgen -t seqgen_test.tre >/dev/null && cd ..

cd pxseqgen_example && echo pxseqgen && pxseqgen -t seqgen_test.tre -o output_alignment -m .33,.33,.33,.33,.33,.33,2,.3,.3,.2,.5,.4,.2 >/dev/null && cd ..

cd pxsstat_example && echo pxsstat && pxsstat -s Bollback_2002_MBE.fa >/dev/null && cd ..

cd pxstrec_example && echo pxstrec && pxstrec -d test.data.narrow -t test.tre -c config_stmap >/dev/null && cd ..

cd pxsw_example && echo pxsw && pxsw -s Alignment.fa >/dev/null && cd ..

cd pxt2new_example && echo pxt2new && pxt2new -t Tree.nex >/dev/null && cd ..

cd pxtlate_example && echo pxtlate && pxtlate -s Sequence.fa >/dev/null && cd ..

cd pxtscale_example && echo pxtscale && pxtscale -t ultra.tre -s 2.0 >/dev/null && cd ..

cd pxupgma_example && echo pxupgma && pxupgma -s drosophila.aln >/dev/null && cd ..

cd pxvcf2fa_example && echo pxvcf2fa && pxvcf2fa -s vcf_file >/dev/null && cd ..
